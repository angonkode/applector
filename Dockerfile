FROM golang:1.19.1-alpine AS builder

RUN apk --update add --no-cache git && rm -f /var/cache/apk/*

WORKDIR /tmp/applector/goconfig
COPY goconfig .
RUN mkdir -p /opt
RUN cd cmd/appdebug && go get -v && go clean && go build && cp appdebug /opt/
RUN cd cmd/sshgen && go get -v && go clean && go build && cp sshgen /opt/

WORKDIR /opt
COPY scripts .

FROM alpine:3.16
LABEL Author="Agus Purnomo <watonist@gmail.com>"
LABEL Description="Application reflector, a containerized local/remote tunneling service"

RUN apk --update add --no-cache openssh-client && rm -f /var/cache/apk/* \
 && ln -s /usr/bin/ssh /opt/applector

WORKDIR /opt
COPY --from=builder /opt .
RUN chmod -R u+rwX,og+rX,og-w /opt
RUN mkdir ~/.ssh && touch ~/.ssh/config && chmod -R og-rwx ~/.ssh
RUN echo 'Host *' >> /etc/ssh/ssh_config \
 && echo '  Compression yes' >> /etc/ssh/ssh_config \
 && echo '  IdentitiesOnly yes' >> /etc/ssh/ssh_config \
 && echo '  GatewayPorts yes' >> /etc/ssh/ssh_config \
 && echo '  ExitOnForwardFailure yes' >> /etc/ssh/ssh_config \
 && echo '  ForwardAgent no' >> /etc/ssh/ssh_config \
 && echo '  ForwardX11 no' >> /etc/ssh/ssh_config
# && echo '  UseRoaming no' >> /etc/ssh/ssh_config

ENTRYPOINT ["/opt/entrypoint.sh"]
