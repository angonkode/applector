# applector

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/angonkode/applector)](https://goreportcard.com/report/gitlab.com/angonkode/applector)
[![Pipeline](https://gitlab.com/angonkode/applector/badges/master/pipeline.svg)](./)

Application reflector, a containerized local/remote tunneling service.

[sample docker compose](docker-compose.yml.example)

## Project builds

login to docker hub

```sh
docker login -u <username> registry.gitlab.com
```

build container

```sh
cd <project working directory>
docker build -t registry.gitlab.com/angonkode/applector:test .
docker tag registry.gitlab.com/angonkode/applector:test angonkomputer/applector:test
```

upload container

```sh
docker push registry.gitlab.com/angonkode/applector:test
docker push angonkomputer/applector:test
```
