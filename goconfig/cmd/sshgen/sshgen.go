package main

import (
	"fmt"

	"gitlab.com/angonkode/apps-container/applector.git/goconfig/modules/applector"
)

func sshConfigGen() error {
	proxies := applector.ListProxies()
	for _, proxyName := range proxies {
		proxy := applector.GetProxyByName(proxyName)

		fmt.Println("Host", proxy.Name)
		fmt.Println("  Hostname", proxy.Hostname)
		fmt.Println("  Port", proxy.Port)
		if proxy.HasUser() {
			fmt.Println("  User", proxy.User)
		}
		if proxy.HasProxy() {
			fmt.Println("  ProxyJump", proxy.Proxy)
		}
		if proxy.HasKeys() {
			fmt.Println("  IdentitiesOnly", "yes")
			for _, k := range proxy.Keys {
				fmt.Println("  IdentityFile", applector.GetSSHKeyByName(k).File)
			}
		}
		fmt.Println()
	}

	appl := applector.GetApplector()
	fmt.Println("Host startup")
	fmt.Println("  Hostname", appl.Hostname)
	fmt.Println("  Port", appl.Port)
	if appl.HasUser() {
		fmt.Println("  User", appl.User)
	}
	if appl.HasProxy() {
		fmt.Println("  ProxyJump", appl.Proxy)
	}
	if appl.HasKeys() {
		fmt.Println("  IdentitiesOnly", "yes")
		for _, k := range appl.Keys {
			fmt.Println("  IdentityFile", applector.GetSSHKeyByName(k).File)
		}
	}
	services := applector.ListServices()
	if len(services) > 0 {
		for _, serviceName := range services {
			service := applector.GetServiceByName(serviceName)
			if service == nil {
				continue
			}
			if service.Mode == "REMOTE" {
				fmt.Println("  RemoteForward",
					fmt.Sprintf("%s:%d", service.Source.GetHost(), service.Source.GetPort()),
					fmt.Sprintf("%s:%d", service.Destination.GetHost(), service.Destination.GetPort()))
				continue
			}
			if service.Mode == "LOCAL" {
				fmt.Println("  LocalForward",
					fmt.Sprintf("%s:%d", service.Source.GetHost(), service.Source.GetPort()),
					fmt.Sprintf("%s:%d", service.Destination.GetHost(), service.Destination.GetPort()))
				continue
			}
			if service.Mode == "DYNAMIC" {
				fmt.Println("  DynamicForward",
					fmt.Sprintf("%s:%d", service.Source.GetHost(), service.Source.GetPort()),
				)
			}
		}
	}
	fmt.Println()

	fmt.Println("Host", "*")
	fmt.Println("  Compression", "yes")
	fmt.Println("  GatewayPorts", "yes")
	fmt.Println("  ExitOnForwardFailure", "yes")
	fmt.Println("  ForwardAgent", "no")
	fmt.Println("  ForwardX11", "no")
	// fmt.Println("  UseRoaming", "no")
	fmt.Println("  StrictHostKeyChecking", "no")
	sshKeys := applector.ListSSHKeys()
	if len(sshKeys) > 0 {
		fmt.Println("  IdentitiesOnly", "yes")
		for _, k := range sshKeys {
			fmt.Println("  IdentityFile", applector.GetSSHKeyByName(k).File)
		}
	}

	return nil
}
