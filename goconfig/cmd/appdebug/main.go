package main

import (
	"fmt"

	"gitlab.com/angonkode/apps-container/applector.git/goconfig/utils"
)

func main() {
	if utils.IsDebug() {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
