package applector

import "gitlab.com/angonkode/apps-container/applector.git/goconfig/utils"

// SSHKey SSH key
type SSHKey struct {
	Name string
	File string
}

func newKey(name, keySpec string) SSHKey {
	return SSHKey{
		Name: name,
		File: utils.Trim(keySpec),
	}
}
