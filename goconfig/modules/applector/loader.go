package applector

import (
	"fmt"
	"os"

	"gitlab.com/angonkode/apps-container/applector.git/goconfig/modules/log"
)

const applectorKey string = "APPLECTOR"
const servicePrefix string = "SERVICE"

// LoadEnv load configuration from environment variables
func LoadEnv() {
	logger := log.Logger()
	applectorHost, applectorExist := os.LookupEnv(applectorKey)
	if !applectorExist {
		logger.Fatalln("Key not found:", applectorKey)
	}

	applector = newHost(applectorKey, applectorHost)
	for {
		proxies := findUnloadedProxies()
		if len(proxies) == 0 {
			break
		}

		for _, proxyKey := range proxies {
			proxySpec, proxyExist := os.LookupEnv(proxyKey)
			if !proxyExist {
				logger.Fatalln("Unable to load proxy configuration:", proxyKey)
			}
			proxy := newHost(proxyKey, proxySpec)
			applectorProxies[proxyKey] = proxy
		}
	}

	for {
		keys := findUnloadedKeys()
		if len(keys) == 0 {
			break
		}

		for _, keyname := range keys {
			keySpec, keyExist := os.LookupEnv(keyname)
			if !keyExist {
				logger.Fatalln("Unable to load SSH key configuration:", keyname)
			}
			key := newKey(keyname, keySpec)
			applectorSSHKeys[keyname] = key
		}
	}

	for i := 1; i <= GetMaxService(); i++ {
		keyname := fmt.Sprintf("%s%d", servicePrefix, i)
		keySpec, keyExist := os.LookupEnv(keyname)
		if !keyExist {
			continue
		}
		service := newService(keyname, keySpec)
		applectorServices[keyname] = service
	}
}

func findUnloadedProxies() []string {
	result := []string{}
	if applector.HasProxy() {
		_, keyExist := applectorProxies[applector.Proxy]
		if !keyExist {
			result = append(result, applector.Proxy)
		}
	}

	for proxyName := range applectorProxies {
		proxy, proxyExist := applectorProxies[proxyName]
		if !proxyExist {
			result = append(result, proxyName)
			continue
		}
		if proxy.HasProxy() {
			_, keyExist := applectorProxies[proxy.Proxy]
			if !keyExist {
				result = append(result, proxy.Proxy)
			}
		}
	}

	return result
}

func findUnloadedKeys() []string {
	result := []string{}
	if applector.HasKeys() {
		for _, sshKey := range applector.Keys {
			_, keyExist := applectorSSHKeys[sshKey]
			if !keyExist {
				result = append(result, sshKey)
			}
		}
	}

	for proxyName := range applectorProxies {
		proxy, proxyExist := applectorProxies[proxyName]
		if !proxyExist {
			continue
		}
		if proxy.HasKeys() {
			for _, sshKey := range proxy.Keys {
				_, keyExist := applectorSSHKeys[sshKey]
				if !keyExist {
					result = append(result, sshKey)
				}
			}
		}
	}

	return result
}
