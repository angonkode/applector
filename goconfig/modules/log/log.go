package log

import (
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/angonkode/apps-container/applector.git/goconfig/utils"
)

func init() {
	logrus.SetReportCaller(true)
	if utils.IsDebug() {
		logrus.SetLevel(logrus.DebugLevel)
	} else {
		logrus.SetLevel(logrus.InfoLevel)
	}
	// logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(os.Stderr)
}

// Logger get logger instance
func Logger() *logrus.Entry {
	return logrus.WithFields(logrus.Fields{
		"pid":  os.Getpid(),
		"ppid": os.Getppid(),
	})
}
